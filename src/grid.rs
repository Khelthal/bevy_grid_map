use bevy::{prelude::*, utils::HashMap};

// plugin

pub struct GridPlugins;

impl PluginGroup for GridPlugins {
    fn build(&mut self, group: &mut bevy::app::PluginGroupBuilder) {
        group.add(GridBasePlugin);
        group.add(GridCleanCollisionsPlugin);
    }
}

pub struct GridBasePlugin;

impl Plugin for GridBasePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(CoreStage::PreUpdate, move_grid_elements)
            .add_system(add_new_elements);
    }
}

pub struct GridCleanCollisionsPlugin;

impl Plugin for GridCleanCollisionsPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(CoreStage::PostUpdate, clean_collisions);
    }
}

// constants

// structs

#[derive(Default, Debug, Component)]
pub struct GridMap {
    data: Vec<Vec<Vec<GridData>>>,
}

impl GridMap {
    pub fn new(width: usize, height: usize) -> Self {
        if width == 0 || height == 0 {
            panic!("Width and height have to be greater than 0");
        }

        let data = (0..height)
            .into_iter()
            .map(|_| (0..width).into_iter().map(|_| Vec::new()).collect())
            .collect();

        GridMap { data }
    }

    fn check_collisions(&self, entity: Entity, position: &GridPosition) -> Option<GridCollisions> {
        let colliding_entities: Vec<Entity> = self.data[position.1][position.0]
            .iter()
            .filter_map(|&elem| {
                if let GridData::Accesible(cont_entity) = elem {
                    if cont_entity != entity {
                        return Some(cont_entity);
                    }
                }
                None
            })
            .collect();

        if colliding_entities.len() > 0 {
            return Some(GridCollisions(colliding_entities));
        }
        None
    }

    fn move_element(
        &mut self,
        entity: Entity,
        mut position: Mut<GridPosition>,
        movement: &GridMove,
    ) -> Option<GridCollisions> {
        let direction = match movement {
            GridMove::Up => (0, 1),
            GridMove::Down => (0, -1),
            GridMove::Left => (-1, 0),
            GridMove::Right => (1, 0),
        };
        let original_position = (position.0 as i32, position.1 as i32);
        let mut destiny_position = original_position;
        destiny_position.0 += direction.0;
        destiny_position.1 += direction.1;

        let mut can_move = true;

        if !self.is_on_boundaries(destiny_position) {
            can_move = false;
        }

        can_move = if can_move {
            let destiny_grid =
                &mut self.data[destiny_position.1 as usize][destiny_position.0 as usize];
            !destiny_grid
                .iter()
                .any(|grid_data| matches!(grid_data, GridData::Inaccesible))
        } else {
            false
        };

        if can_move {
            let current_grid = &mut self.data[position.1][position.0];
            let current_data: GridData = current_grid.swap_remove(
                current_grid
                    .iter()
                    .position(|&elem| {
                        if let GridData::Accesible(cont_entity) = elem {
                            if cont_entity == entity {
                                return true;
                            }
                        }
                        false
                    })
                    .unwrap(),
            );
            self.data[destiny_position.1 as usize][destiny_position.0 as usize].push(current_data);

            position.0 = destiny_position.0 as usize;
            position.1 = destiny_position.1 as usize;
        }

        self.check_collisions(entity, &position)
    }

    fn is_on_boundaries(&self, position: (i32, i32)) -> bool {
        if position.0 < 0 || position.0 >= self.width() as i32 {
            return false;
        }

        if position.1 < 0 || position.1 >= self.height() as i32 {
            return false;
        }

        true
    }

    fn width(&self) -> usize {
        self.data[0].len()
    }

    fn height(&self) -> usize {
        self.data.len()
    }
}

#[derive(Bundle, Default)]
pub struct GridElementBundle {
    pub grid_position: GridPosition,
    pub grid_collisions: GridCollisions,
}

#[derive(Component, Clone, Copy, Default)]
pub struct GridPosition(pub usize, pub usize);

#[derive(Component, Deref, DerefMut, Default)]
pub struct GridCollisions(Vec<Entity>);

#[derive(Component, Debug)]
pub struct GridInaccesible;

// enums

#[derive(Component)]
pub enum GridMove {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone, Copy)]
enum GridData {
    Accesible(Entity),
    Inaccesible,
}

// systems

fn move_grid_elements(
    grid_map: Option<ResMut<GridMap>>,
    mut commands: Commands,
    mut grid_elements: Query<(
        Entity,
        &mut GridPosition,
        Option<&GridMove>,
        &mut GridCollisions,
    )>,
) {
    if let Some(mut grid_map) = grid_map {
        let mut unapplied_collisions: HashMap<Entity, Vec<Entity>> = HashMap::new();
        grid_elements
            .iter_mut()
            .filter(|(_, _, grid_move, _)| grid_move.is_some())
            .for_each(
                |(grid_entity, grid_position, grid_move, mut grid_collisions)| {
                    let grid_move = grid_move.unwrap();
                    if let Some(collisions) =
                        grid_map.move_element(grid_entity, grid_position, grid_move)
                    {
                        grid_collisions.extend(collisions.iter());
                        collisions.iter().for_each(|colliding_entity| {
                            if unapplied_collisions.contains_key(colliding_entity) {
                                unapplied_collisions
                                    .get_mut(colliding_entity)
                                    .unwrap()
                                    .push(grid_entity);
                            } else {
                                unapplied_collisions.insert(*colliding_entity, vec![grid_entity]);
                            }
                        });
                    }
                    commands.entity(grid_entity).remove::<GridMove>();
                },
            );
        unapplied_collisions
            .iter()
            .for_each(|(entity, collisions)| {
                let (_, _, _, mut grid_collisions) = grid_elements.get_mut(*entity).unwrap();
                grid_collisions.extend(collisions);
            });
    }
}

fn add_new_elements(
    grid_map: Option<ResMut<GridMap>>,
    mut grid_elements: Query<
        (
            Entity,
            &GridPosition,
            Option<&GridInaccesible>,
            &mut GridCollisions,
        ),
        Added<GridPosition>,
    >,
) {
    if let Some(mut grid_map) = grid_map {
        let mut unapplied_collisions: HashMap<Entity, Vec<Entity>> = HashMap::new();
        grid_elements.iter_mut().for_each(
            |(grid_entity, grid_position, grid_inaccesible, mut grid_collisions)| {
                if !grid_map.is_on_boundaries((grid_position.0 as i32, grid_position.1 as i32)) {
                    panic!("Invalid position");
                }
                let destiny_grid = &mut grid_map.data[grid_position.1][grid_position.0];

                if destiny_grid
                    .iter()
                    .any(|grid_data| matches!(grid_data, GridData::Inaccesible))
                {
                    panic!("Inaccesible position");
                }

                match grid_inaccesible {
                    Some(_) => {
                        if destiny_grid.len() > 0 {
                            panic!("Inaccesible position");
                        }
                        destiny_grid.push(GridData::Inaccesible)
                    }
                    None => destiny_grid.push(GridData::Accesible(grid_entity)),
                }

                if let Some(collisions) = grid_map.check_collisions(grid_entity, grid_position) {
                    grid_collisions.extend(collisions.iter());
                    collisions.iter().for_each(|colliding_entity| {
                        if unapplied_collisions.contains_key(colliding_entity) {
                            unapplied_collisions
                                .get_mut(colliding_entity)
                                .unwrap()
                                .push(grid_entity);
                        } else {
                            unapplied_collisions.insert(*colliding_entity, vec![grid_entity]);
                        }
                    });
                }
            },
        );
        unapplied_collisions
            .iter()
            .for_each(|(entity, collisions)| {
                let (_, _, _, mut grid_collisions) = grid_elements.get_mut(*entity).unwrap();
                grid_collisions.extend(collisions);
            });
    }
}

fn clean_collisions(mut grid_colliding_elements: Query<&mut GridCollisions>) {
    grid_colliding_elements
        .iter_mut()
        .for_each(|mut collisions| {
            collisions.clear();
        });
}
