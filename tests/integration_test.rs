use bevy::prelude::*;
use bevy_grid_map::*;

#[test]
fn instance_grid_elements() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });
    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });
    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });

    app.update();
}

#[test]
#[should_panic]
fn instance_grid_elements_colliding_first() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    app.world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .insert(GridInaccesible);
    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });
    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });

    app.update();
}

#[test]
#[should_panic]
fn instance_grid_elements_colliding_last() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });
    app.world.spawn().insert_bundle(GridElementBundle {
        grid_position: GridPosition(0, 0),
        ..Default::default()
    });
    app.world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .insert(GridInaccesible);

    app.update();
}

#[test]
fn collide_elements() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    let entity_2 = app
        .world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .id();
    let entity_1 = app
        .world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .id();

    app.update();

    assert_eq!(app.world.get::<GridCollisions>(entity_1).unwrap().len(), 1);
    assert_eq!(app.world.get::<GridCollisions>(entity_2).unwrap().len(), 1);
}

#[test]
fn move_and_collide_elements() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    let standing_entity = app
        .world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .id();
    let moving_entity = app
        .world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(1, 0),
            ..Default::default()
        })
        .id();

    app.update();

    app.world.entity_mut(moving_entity).insert(GridMove::Left);

    app.update();

    assert_eq!(
        app.world
            .get::<GridCollisions>(moving_entity)
            .unwrap()
            .len(),
        1
    );
    assert_eq!(
        app.world
            .get::<GridCollisions>(standing_entity)
            .unwrap()
            .len(),
        1
    );
}

#[test]
fn check_boundaries() {
    let mut app = App::new();

    app.add_plugin(GridBasePlugin);
    app.insert_resource(GridMap::new(3, 3));

    let moving_entity = app
        .world
        .spawn()
        .insert_bundle(GridElementBundle {
            grid_position: GridPosition(0, 0),
            ..Default::default()
        })
        .id();

    app.update();

    app.world.entity_mut(moving_entity).insert(GridMove::Left);

    app.update();

    assert_eq!(app.world.get::<GridPosition>(moving_entity).unwrap().0, 0);
}
